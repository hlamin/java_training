package solution.exercise.java_data_types;

import java.util.Scanner;

/*
Write a Java program to test a number is positive or negative.
Test Data
Input number: 35
*/
public class Solution_No001 {

	public static void main(String[] args) {

		// Variable declaration
		Scanner in = new Scanner(System.in);
		
		// Process Start
		System.out.print("Input number : ");

		// Scan integer input
		int input = in.nextInt();

		// if input greater than zero
		if (input > 0) {
			System.out.println("Number is positive");
		}
		// if input less than zero
		else if (input < 0) {
			System.out.println("Number is negative");
		}
		// if non of above
		else {
			System.out.println("Number is zero");
		}
	}

}
